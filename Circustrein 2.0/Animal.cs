﻿using System.Collections.Generic;

namespace Circustrein_2._0
{
    public enum Type
    {
        Herbivoor,
        Carnivoor
    }
    public enum Size
    {
        Small,
        Medium,
        Large
    }

     public class Animal
    {
        public int Points { get; }

        public Type Type { get; }

        public Size Size { get; }

        public static List<Animal> Herbivoren;
        public static List<Animal> Carnivors;
        public static List<Animal> Allanimals = new List<Animal>();

        public Animal(Type type, Size size)
        {
            if (size == Size.Small)
            {
                Points = 1;
            }

            if (size == Size.Medium)
            {
                Points = 3;
            }

            if (size == Size.Large)
            {
                Points = 5;
            }
            this.Type = type;
            this.Size = size;
        }

        public static void SplitAnimals()
        {
            Carnivors = new List<Animal>();
            Herbivoren = new List<Animal>();

            foreach (Animal carni in Allanimals)
            {
                bool addcarnivor = (carni.Type == Type.Carnivoor);

                if (addcarnivor)
                {
                    Carnivors.Add(carni);
                }
            }

            foreach (var herbi in Allanimals)
            {
                bool addherbivor = (herbi.Type == Type.Herbivoor);

                if (addherbivor)
                {
                    Herbivoren.Add(herbi);
                }
            }
        }

        public override string ToString()
        {
            return Type + " " + Size + " ";
        }
    }
}
