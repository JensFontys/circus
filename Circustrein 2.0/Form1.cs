﻿using System;
using System.Windows.Forms;

namespace Circustrein_2._0
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            Dieren.DataSource = Enum.GetValues(typeof(Type));
            grootte.DataSource = Enum.GetValues(typeof(Size));
        }

        private void Voegtoeknop_Click(object sender, EventArgs e)
        {
            string type = Dieren.Text;
            var tp = (Type)Enum.Parse(typeof(Type), type);
            string size = grootte.Text;
            var sz = (Size) Enum.Parse(typeof(Size), size);

            var a = new Animal(tp, sz);
            Animal.Allanimals.Add(a);
            Allanimalslistbox.Items.Clear();
            foreach (var v in Animal.Allanimals)
            {
                Allanimalslistbox.Items.Add(v);
            }
        }

        private void Sortbtn_Click(object sender, EventArgs e)
        {
            trainlistbox.Items.Clear();
            Animal.SplitAnimals();
            Wagon.AddAnimals();
            Wagon.AddRemainingherbivoren();
            foreach (var wagon in Train.Wagons)
            {
                trainlistbox.Items.Add(wagon);
            }
        }
    }
}