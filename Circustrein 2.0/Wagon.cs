﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Circustrein_2._0
{
     public class Wagon
    {
        private List<Animal> _wagonAnimals = new List<Animal>();

        public int Points { get; set; }

        public List<Animal> animalwagonList
        {
            get { return _wagonAnimals; }
            set { _wagonAnimals = value; }
        }

        public static void AddAnimals()
        {
            Animal.Carnivors.Sort((a, b) => b.Points.CompareTo(a.Points));
            foreach (var b in Animal.Carnivors.ToList())
            {
                Wagon Wgn = new Wagon();
                Wgn.animalwagonList.Add(b);
                Wgn.Points = Wgn.Points + b.Points;
                Animal.Carnivors.Remove(b);
                foreach (var h in Animal.Herbivoren.ToList())
                {
                    if (Wgn.Points + h.Points <= 10 && h.Points > b.Points)
                    {
                        Wgn.animalwagonList.Add(h);
                        Wgn.Points = Wgn.Points + h.Points;
                        Animal.Herbivoren.Remove(h);
                    }
                }
                Train.Wagons.Add(Wgn);
            }
        }

        public static void AddRemainingherbivoren()
        {
            var wgn = new Wagon();
            Animal.Herbivoren.Sort((a, b) => b.Points.CompareTo(a.Points));
            while (Animal.Herbivoren.Count != 0)
            {
                for (int i = 0; i < Animal.Herbivoren.Count; i++)
                {
                    if (wgn.Points + Animal.Herbivoren[i].Points <= 10)
                    {
                        wgn.Points = wgn.Points + Animal.Herbivoren[i].Points;
                        wgn.animalwagonList.Add(Animal.Herbivoren[i]);
                        Animal.Herbivoren.Remove(Animal.Herbivoren[i]);
                        i -= 1;
                    }
                }
                Train.Wagons.Add(wgn);
                wgn = new Wagon();
            }
        }

        public override string ToString()
        {
            return "Wagon: " + String.Concat(_wagonAnimals) + " - Total Points: " + Points;
        }
    }
}
