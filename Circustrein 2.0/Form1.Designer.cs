﻿namespace Circustrein_2._0
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Dieren = new System.Windows.Forms.ComboBox();
            this.typelabel = new System.Windows.Forms.Label();
            this.grootte = new System.Windows.Forms.ComboBox();
            this.groottelabel = new System.Windows.Forms.Label();
            this.Voegtoeknop = new System.Windows.Forms.Button();
            this.Allanimalslistbox = new System.Windows.Forms.ListBox();
            this.Animallabel = new System.Windows.Forms.Label();
            this.trainlistbox = new System.Windows.Forms.ListBox();
            this.trainlabel = new System.Windows.Forms.Label();
            this.Sortbtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Dieren
            // 
            this.Dieren.FormattingEnabled = true;
            this.Dieren.Location = new System.Drawing.Point(12, 34);
            this.Dieren.Name = "Dieren";
            this.Dieren.Size = new System.Drawing.Size(121, 21);
            this.Dieren.TabIndex = 0;
            // 
            // typelabel
            // 
            this.typelabel.AutoSize = true;
            this.typelabel.Location = new System.Drawing.Point(13, 18);
            this.typelabel.Name = "typelabel";
            this.typelabel.Size = new System.Drawing.Size(34, 13);
            this.typelabel.TabIndex = 1;
            this.typelabel.Text = "Type:";
            // 
            // grootte
            // 
            this.grootte.FormattingEnabled = true;
            this.grootte.Location = new System.Drawing.Point(12, 94);
            this.grootte.Name = "grootte";
            this.grootte.Size = new System.Drawing.Size(121, 21);
            this.grootte.TabIndex = 2;
            // 
            // groottelabel
            // 
            this.groottelabel.AutoSize = true;
            this.groottelabel.Location = new System.Drawing.Point(13, 78);
            this.groottelabel.Name = "groottelabel";
            this.groottelabel.Size = new System.Drawing.Size(30, 13);
            this.groottelabel.TabIndex = 3;
            this.groottelabel.Text = "Size:";
            // 
            // Voegtoeknop
            // 
            this.Voegtoeknop.Location = new System.Drawing.Point(12, 139);
            this.Voegtoeknop.Name = "Voegtoeknop";
            this.Voegtoeknop.Size = new System.Drawing.Size(121, 23);
            this.Voegtoeknop.TabIndex = 4;
            this.Voegtoeknop.Text = "Add";
            this.Voegtoeknop.UseVisualStyleBackColor = true;
            this.Voegtoeknop.Click += new System.EventHandler(this.Voegtoeknop_Click);
            // 
            // Allanimalslistbox
            // 
            this.Allanimalslistbox.FormattingEnabled = true;
            this.Allanimalslistbox.Location = new System.Drawing.Point(162, 34);
            this.Allanimalslistbox.Name = "Allanimalslistbox";
            this.Allanimalslistbox.Size = new System.Drawing.Size(182, 238);
            this.Allanimalslistbox.TabIndex = 5;
            // 
            // Animallabel
            // 
            this.Animallabel.AutoSize = true;
            this.Animallabel.Location = new System.Drawing.Point(159, 18);
            this.Animallabel.Name = "Animallabel";
            this.Animallabel.Size = new System.Drawing.Size(46, 13);
            this.Animallabel.TabIndex = 6;
            this.Animallabel.Text = "Animals:";
            // 
            // trainlistbox
            // 
            this.trainlistbox.FormattingEnabled = true;
            this.trainlistbox.Location = new System.Drawing.Point(350, 34);
            this.trainlistbox.Name = "trainlistbox";
            this.trainlistbox.Size = new System.Drawing.Size(761, 238);
            this.trainlistbox.TabIndex = 7;
            // 
            // trainlabel
            // 
            this.trainlabel.AutoSize = true;
            this.trainlabel.Location = new System.Drawing.Point(347, 18);
            this.trainlabel.Name = "trainlabel";
            this.trainlabel.Size = new System.Drawing.Size(34, 13);
            this.trainlabel.TabIndex = 8;
            this.trainlabel.Text = "Train:";
            // 
            // Sortbtn
            // 
            this.Sortbtn.Location = new System.Drawing.Point(12, 249);
            this.Sortbtn.Name = "Sortbtn";
            this.Sortbtn.Size = new System.Drawing.Size(121, 23);
            this.Sortbtn.TabIndex = 11;
            this.Sortbtn.Text = "Sort";
            this.Sortbtn.UseVisualStyleBackColor = true;
            this.Sortbtn.Click += new System.EventHandler(this.Sortbtn_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1159, 307);
            this.Controls.Add(this.Sortbtn);
            this.Controls.Add(this.trainlabel);
            this.Controls.Add(this.trainlistbox);
            this.Controls.Add(this.Animallabel);
            this.Controls.Add(this.Allanimalslistbox);
            this.Controls.Add(this.Voegtoeknop);
            this.Controls.Add(this.groottelabel);
            this.Controls.Add(this.grootte);
            this.Controls.Add(this.typelabel);
            this.Controls.Add(this.Dieren);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox Dieren;
        private System.Windows.Forms.Label typelabel;
        private System.Windows.Forms.ComboBox grootte;
        private System.Windows.Forms.Label groottelabel;
        private System.Windows.Forms.Button Voegtoeknop;
        private System.Windows.Forms.ListBox Allanimalslistbox;
        private System.Windows.Forms.Label Animallabel;
        private System.Windows.Forms.ListBox trainlistbox;
        private System.Windows.Forms.Label trainlabel;
        private System.Windows.Forms.Button Sortbtn;
    }
}

