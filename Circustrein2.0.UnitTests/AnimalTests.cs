﻿using Circustrein_2._0;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Type = Circustrein_2._0.Type;

namespace Circustrein2._0.UnitTests
{
    [TestClass]
    public class AnimalTests
    {
        [TestMethod]
        public void Animal_SizeIsSmall_PointsIs1()
        {
            //Arrange
            var animal = new Animal(Type.Carnivoor, Size.Small);
            //Act
            bool result = (animal.Points == 1);
            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void Animal_SizeIsMedium_PointsIs3()
        {
            //Arrange
            var animal = new Animal(Type.Carnivoor, Size.Medium);
            //Act
            bool result = (animal.Points == 4);
            //Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Animal_SizeIsLarge_PointsIs5()
        {
            //Arrange
            var animal = new Animal(Type.Carnivoor, Size.Large);
            //Act
            bool result = (animal.Points == 5);
            //Assert
            Assert.IsTrue(result);
        }
    }

    [TestClass]
    public class TrainTests
    {
        [TestMethod]
        public void SplitAnimals_SplitAllAnimalList_HerbivorAndCarnivorList()
        {
            //Arrange
            var carnivor1 = new Animal(Type.Carnivoor, Size.Medium);
            var carnivor2 = new Animal(Type.Carnivoor, Size.Medium);
            var carnivor3 = new Animal(Type.Carnivoor, Size.Medium);
            var herbivor1 = new Animal(Type.Herbivoor, Size.Medium);
            var herbivor2 = new Animal(Type.Herbivoor, Size.Medium);
            var herbivor3 = new Animal(Type.Herbivoor, Size.Medium);

            Animal.Allanimals.Add(carnivor1);
            Animal.Allanimals.Add(carnivor2);
            Animal.Allanimals.Add(carnivor3);
            Animal.Allanimals.Add(herbivor1);
            Animal.Allanimals.Add(herbivor2);
            Animal.Allanimals.Add(herbivor3);

            List<Animal> herbi = new List<Animal> {herbivor1, herbivor2, herbivor3};

            //Act
            Animal.SplitAnimals();

            //Assert
            Assert.AreEqual(herbi.Count, Animal.Herbivoren.Count);
            Animal.Allanimals.Clear();
            Animal.Carnivors.Clear();
            Animal.Herbivoren.Clear();
        }

        [TestMethod]
        public void AddCarnivorWagons_WagonListFullCarnivor_AddWagon()
        {
            //Arrange
            var carnivor1 = new Animal(Type.Carnivoor, Size.Small);
            var carnivor2 = new Animal(Type.Carnivoor, Size.Medium);
            var carnivor3 = new Animal(Type.Carnivoor, Size.Large);
            var carnivor4 = new Animal(Type.Carnivoor, Size.Medium);

            Animal.Carnivors.Add(carnivor1);
            Animal.Carnivors.Add(carnivor2);
            Animal.Carnivors.Add(carnivor3);
            Animal.Carnivors.Add(carnivor4);

            List<Animal> animals = new List<Animal> {carnivor1, carnivor2, carnivor3, carnivor4};
            List<Wagon> wagon = new List<Wagon>();
            var wgn = new Wagon();

            foreach (var a in animals)
            {
                wagon.Add(wgn);
            }

            //Act
            Wagon.AddAnimals();

            //Assert
            Assert.AreEqual(wagon.Count, Train.Wagons.Count);
            Animal.Allanimals.Clear();
            Animal.Carnivors.Clear();
            Animal.Herbivoren.Clear();
            Train.Wagons.Clear();
        }

        [TestMethod]
        public void Addherbivoren_WagonlistFullHerbivor_AddWagon()
        {
            //Arrange
            var herbivor1 = new Animal(Type.Herbivoor, Size.Medium);
            var herbivor2 = new Animal(Type.Herbivoor, Size.Medium);
            var herbivor3 = new Animal(Type.Herbivoor, Size.Medium);

            Animal.Herbivoren.Add(herbivor1);
            Animal.Herbivoren.Add(herbivor2);
            Animal.Herbivoren.Add(herbivor3);

            List<Wagon> wagon = new List<Wagon>();
            var wgn = new Wagon();
            wagon.Add(wgn);

            //Act
            Wagon.AddRemainingherbivoren();

            //Assert
            Assert.AreEqual(wagon.Count, Train.Wagons.Count);
        }
    }
}
